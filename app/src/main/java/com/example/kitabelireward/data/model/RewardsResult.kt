package com.example.kitabelireward.data.model

data class RewardsResult(
    val result: Boolean,
    val data: RewardsData?
)