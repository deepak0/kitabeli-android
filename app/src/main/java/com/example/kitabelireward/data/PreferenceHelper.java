package com.example.kitabelireward.data;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {

    private Context context;
    private SharedPreferences sharedPreferences;
    //private SharedPreferences.Editor editor;
    private static PreferenceHelper preferenceHelper;

    private PreferenceHelper(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences("kitaBeliRewards", Context.MODE_PRIVATE);
        //editor = sharedPreferences.edit();
    }

    public static PreferenceHelper getPreferenceHelper(Context context){
        if (preferenceHelper == null){
            preferenceHelper = new PreferenceHelper(context);
        }
        return preferenceHelper;
    }

    public SharedPreferences getSharedPreferences(){
        return preferenceHelper.sharedPreferences;
    }
}
