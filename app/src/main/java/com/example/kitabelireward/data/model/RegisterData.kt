package com.example.kitabelireward.data.model

data class RegisterData(
    val name: String,
    val email: String
)