package com.example.kitabelireward.data.model

data class LoginUserData(
    val name: String,
    val email: String,
    val password: String?
)