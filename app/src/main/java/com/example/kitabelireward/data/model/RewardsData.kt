package com.example.kitabelireward.data.model

data class RewardsData(
    val email: String,
    val time: String,
    val win: Boolean
)