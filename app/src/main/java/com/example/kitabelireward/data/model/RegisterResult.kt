package com.example.kitabelireward.data.model

data class RegisterResult(
    val result: Boolean,
    val data: RegisterData?
)