package com.example.kitabelireward

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.kitabelireward.api.RetrofitClient
import com.example.kitabelireward.data.PreferenceHelper
import com.example.kitabelireward.data.model.RewardsResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [RewardsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [RewardsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RewardsFragment : Fragment() {
    // TODO: Rename and change types of parameters
//    private var param1: String? = null
//    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private val PREFS_USER: String = "User"
    private val PREFS_LOGIN_STATE: String = "isLoggedIn"

    private lateinit var navController: NavController
    private lateinit var sharedPreferences: SharedPreferences

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }
//    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rewards, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        val sharedPreferences = PreferenceHelper.getPreferenceHelper(context).sharedPreferences
        var rewardsPoints = sharedPreferences.getInt("rewardsPoints", 0)
        val userName : String? = sharedPreferences.getString("user.name", "")
        val userEmail : String? = sharedPreferences.getString("user.email", "")
        val title = view.findViewById<TextView>(R.id.title)
        title.text = ("Welcome $userName to " + resources.getString(R.string.rewards_title))
        val counter = view.findViewById<TextView>(R.id.rewards_counter)
        counter.text = (resources.getString(R.string.rewards_counter) + " $rewardsPoints")
        val play = view.findViewById<Button>(R.id.play)

        play.setOnClickListener {
            play.isEnabled = false
            RetrofitClient.instance.rewards(userEmail!!, "dummy_time", false)
                .enqueue(object: Callback<RewardsResult>{
                    override fun onFailure(call: Call<RewardsResult>, t: Throwable) {
                        Toast.makeText(context, "Better luck next time :(", Toast.LENGTH_SHORT).show()
                        play.isEnabled = true
                    }

                    override fun onResponse(
                        call: Call<RewardsResult>,
                        response: Response<RewardsResult>
                    ) {
                        val userWon: Boolean = response.isSuccessful &&
                                (if (response.body() != null) (
                                        response.body()?.result!! && response.body()?.data!!.win
                                        ) else false)
                        if (userWon) {
                            rewardsPoints += 1
                            val editor = sharedPreferences.edit()
                            editor.putInt("rewardsPoints", rewardsPoints)
                            editor.apply()
                            counter.text = (resources.getString(R.string.rewards_counter) + " $rewardsPoints")
                            Toast.makeText(context, "Hurray!! You Won :)", Toast.LENGTH_SHORT)
                                .show()
                        } else {
                            Toast.makeText(
                                context, "Better luck next time :(", Toast.LENGTH_SHORT).show()
                        }
                        play.isEnabled = true
                    }
                })
        }
    }

//    // TODO: Rename method, update argument and hook method into UI event
//    fun onButtonPressed(uri: Uri) {
//        listener?.onFragmentInteraction(uri)
//    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }



    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RewardsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RewardsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
