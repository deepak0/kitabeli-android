package com.example.kitabelireward.api

import com.example.kitabelireward.data.model.RegisterResult
import com.example.kitabelireward.data.model.RewardsResult
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface Api {

    @FormUrlEncoded
    @POST("/api/register")
    fun register(@Field("name") name: String,
                 @Field("email") email: String,
                 @Field("password") password: String
    ): Call<RegisterResult>

    @FormUrlEncoded
    @POST("/api/rewards")
    fun rewards(@Field("email") email: String,
                 @Field("time") time: String,
                 @Field("win") win: Boolean
    ): Call<RewardsResult>

}